/*!
Isotope value definitions and associated handle types
*/

use parking_lot::{RwLock, RwLockReadGuard as RG, RwLockWriteGuard as WG};
use enumset::{EnumSet, EnumSetType};
use smallvec::SmallVec;
use std::convert::TryFrom;
use std::sync::{Arc, Weak};
use derive_more::From;

/// An isotope value handle
#[derive(Debug, Clone)]
pub struct ValId<S>(Option<Arc<RwLock<Value<S>>>>);

impl<S> ValId<S> {
    /// Get nil
    #[inline] pub fn nil() -> ValId<S> { ValId(None) }
    /// Get the kind of this value
    #[inline]
    pub fn kind(&self) -> DefKind {
        self.0.as_ref()
        .map(|v| v.read().kind())
        // Nil is an empty expr
        .unwrap_or(DefKind::Expr)
    }
    /// Lock a read guard of this value, or `None` for nil
    #[inline] pub fn read(&self) -> Option<RG<Value<S>>> { self.0.as_ref().map(|v| v.read()) }
    /// Lock a write guard of this value, or `None` for nil
    #[inline] pub fn write(&self) -> Option<WG<Value<S>>> { self.0.as_ref().map(|v| v.write()) }
}

impl<S> From<Value<S>> for ValId<S> {
    #[inline] fn from(v: Value<S>) -> ValId<S> { ValId::from(NonNil::from(v)) }
}

/// A non-nil isotope value handle
#[derive(Debug, Clone)]
pub struct NonNil<S>(Arc<RwLock<Value<S>>>);

impl<S> From<Value<S>> for NonNil<S> {
    #[inline] fn from(v: Value<S>) -> NonNil<S> { NonNil(Arc::new(RwLock::new(v))) }
}

impl<S> NonNil<S> {
    /// Get the kind of this value
    #[inline] pub fn kind(&self) -> DefKind { self.0.read().kind() }
    /// Lock a read guard of this value
    #[inline] pub fn read(&self) -> RG<Value<S>> { self.0.read() }
    /// Lock a write guard of this value
    #[inline] pub fn write(&self) -> WG<Value<S>> { self.0.write() }
}

/// A weak isotope value handle
#[derive(Debug, Clone)]
pub struct DefId<S>(Option<Weak<RwLock<Value<S>>>>);

/// A non-nil weak isotope value handle
#[derive(Debug, Clone)]
pub struct NonNilDef<S>(Weak<RwLock<Value<S>>>);

impl<S> From<NonNil<S>> for ValId<S> { #[inline] fn from(n: NonNil<S>) -> ValId<S> { ValId(Some(n.0)) } }
impl<S> From<NonNilDef<S>> for DefId<S> { #[inline] fn from(n: NonNilDef<S>) -> DefId<S> { DefId(Some(n.0)) } }
impl<S> TryFrom<ValId<S>> for NonNil<S> {
    type Error = ();
    #[inline]
    fn try_from(v: ValId<S>) -> Result<NonNil<S>, ()> { v.0.map(|n| NonNil(n)).ok_or(()) }
}
impl<S> TryFrom<DefId<S>> for NonNilDef<S> {
    type Error = ();
    #[inline]
    fn try_from(v: DefId<S>) -> Result<NonNilDef<S>, ()> { v.0.map(|n| NonNilDef(n)).ok_or(()) }
}
impl<S> TryFrom<NonNilDef<S>> for NonNil<S> {
    type Error = ();
    #[inline] fn try_from(d: NonNilDef<S>) -> Result<NonNil<S>, ()> {
        d.0.upgrade()
            .ok_or(())
            .map(|v| NonNil(v))
    }
}
impl<S> TryFrom<DefId<S>> for NonNil<S> {
    type Error = ();
    #[inline] fn try_from(d: DefId<S>) -> Result<NonNil<S>, ()> {
        NonNil::try_from(NonNilDef::try_from(d)?)
    }
}
impl<S> TryFrom<NonNilDef<S>> for ValId<S> {
    type Error = ();
    #[inline] fn try_from(d: NonNilDef<S>) -> Result<ValId<S>, ()> { Ok(NonNil::try_from(d)?.into()) }
}
impl<S> TryFrom<DefId<S>> for ValId<S> {
    type Error = ();
    #[inline] fn try_from(d: DefId<S>) -> Result<ValId<S>, ()> {
        match d.0 {
            Some(d) => ValId::try_from(NonNilDef(d)),
            None => Ok(ValId(None))
        }
    }
}

#[derive(Debug, EnumSetType)]
pub enum ValueFlag {
    /// Whether this value has been type checked
    Checked,
    /// A dummy flag
    Dummy
}

pub type ValueFlags = EnumSet<ValueFlag>;

/// An isotope value
#[derive(Debug, Clone)]
pub struct Value<S> {
    /// The type of this value. It is an error for a value to be it's own type.
    ty: Option<NonNil<S>>,
    /// The flags applying to this value
    flags: ValueFlags,
    /// The definition of this value
    def: Definition<S>
}

impl<S> Value<S> {
    /// Create a value, with a given type
    #[inline]
    pub fn with_ty<D, T>(def: D, ty: T) -> Value<S> where
        D: Into<Definition<S>>, T: Into<NonNil<S>> {
        Self::new(def, Some(ty))
    }
    /// Create a new value, with a given type if any
    #[inline]
    pub fn new<D, T>(def: D, ty: Option<T>) -> Value<S> where
        D: Into<Definition<S>>, T: Into<NonNil<S>> {
        Value { ty: ty.map(|ty| ty.into()), def: def.into(), flags: ValueFlags::empty() }
    }
    /// Get the kind of this value
    #[inline] pub fn kind(&self) -> DefKind { self.def.kind() }
    /// Get the definition of this value
    #[inline] pub fn def(&self) -> &Definition<S> { &self.def }
    /// Set the type of a value, getting the old type, if any
    #[inline] pub fn set_type<T>(&mut self, ty: T) -> Option<NonNil<S>> where T: Into<NonNil<S>> {
        self.ty.replace(ty.into())
    }
    /// Get the type of a value, if any
    #[inline] pub fn get_type(&self) -> Option<&NonNil<S>> { self.ty.as_ref() }
}


impl<S> From<Definition<S>> for Value<S> {
    fn from(def: Definition<S>) -> Value<S> {
        Value {
            ty: None,
            flags: ValueFlags::empty(),
            def
        }
    }
}

/// The kind of an isotope definition
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DefKind {
    Expr,
    Param,
    Lambda,
    Pi,
    Constructor,
    Data,
    Symbol,
    Rec,
    Ind,
    Id,
    Refl,
    Universe
}

/// The definition of an isotope value
#[derive(Debug, Clone)]
#[derive(From)]
pub enum Definition<S> {
    Expr(Expr<S>),
    Param(Param<S>),
    Lambda(Lambda<S>),
    Pi(Pi<S>),
    Constructor(Constructor<S>),
    Data(Data<S>),
    Symbol(Symbol<S>),
    Rec(Recursor<S>),
    Ind(Inductor<S>),
    Id(Identity<S>),
    Refl(Reflexivity<S>),
    Universe(Universe<S>)
}

impl<S> Definition<S> {
    /// Get the kind of this definition
    pub fn kind(&self) -> DefKind {
        use Definition::*;
        match self {
            Expr(_) => DefKind::Expr,
            Param(_) => DefKind::Param,
            Lambda(_) => DefKind::Lambda,
            Pi(_) => DefKind::Pi,
            Constructor(_) => DefKind::Constructor,
            Data(_) => DefKind::Data,
            Symbol(_) => DefKind::Symbol,
            Rec(_) => DefKind::Rec,
            Ind(_) => DefKind::Ind,
            Id(_) => DefKind::Id,
            Refl(_) => DefKind::Refl,
            Universe(_) => DefKind::Universe
        }
    }
}

const SMALL_EXPR_SIZE: usize = 4;

/// An S-expression, evaluated by currying. Arguments are stored in reverse order
/// (i.e. function last)
#[derive(Debug, Clone)]
pub struct Expr<S> {
    pub args: SmallVec<[ValId<S>; SMALL_EXPR_SIZE]>
}

const SMALL_PARAMS_SIZE: usize = 3;

/// A parametrized value
#[derive(Debug, Clone)]
pub struct Parametrized<S> {
    /// This lambda function's parameters
    pub parameters: SmallVec<[NonNil<S>; SMALL_PARAMS_SIZE]>,
    /// This lambda function's result
    pub result: ValId<S>,
    /// This lambda function's self-symbol, if any
    pub self_symbol: Option<NonNil<S>>
}

/// A lambda function
#[derive(Debug, Clone)]
#[derive(From)]
pub struct Lambda<S>(pub Parametrized<S>);

/// A pi type
#[derive(Debug, Clone)]
#[derive(From)]
pub struct Pi<S>(pub Parametrized<S>);

const SMALL_SOURCES_SIZE: usize = 5;

/// A parameter to a parametrized value
#[derive(Debug, Clone)]
pub struct Param<S> {
    /// The values using this parameter
    pub sources: SmallVec<[NonNilDef<S>; SMALL_SOURCES_SIZE]>
}

/// A constructor for an inductive datatype
#[derive(Debug, Clone)]
pub struct Constructor<S>(pub NonNilDef<S>);

const SMALL_CONSTRUCTOR_SIZE: usize = 2;

/// A data declaration
#[derive(Debug, Clone)]
pub struct Data<S> {
    /// The self-symbol of this data declaration
    pub self_symbol: NonNil<S>,
    /// The constructors types of this data declaration.
    /// Should be Constructors of the appropriate type.
    pub constructors: SmallVec<[NonNil<S>; SMALL_CONSTRUCTOR_SIZE]>,
}

/// An isotope symbol
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Symbol<S>(pub S);

/// The recursor of a type
#[derive(Debug, Clone)]
pub struct Recursor<S>(pub ValId<S>);

/// The inductor of a type
#[derive(Debug, Clone)]
pub struct Inductor<S>(pub ValId<S>);

/// The identity type family of a type
#[derive(Debug, Clone)]
pub struct Identity<S>(pub ValId<S>);

/// The reflexivity element of a value in it's associated identity type
#[derive(Debug, Clone)]
pub struct Reflexivity<S>(pub ValId<S>);

const SMALL_CONTAINING_SIZE: usize = 4;

/// A typing universe. Contains a list of universes known to contain it.
/// May take one of these as it's type.
#[derive(Debug, Clone)]
pub struct Universe<S> {
    containing: SmallVec<[NonNil<S>; SMALL_CONTAINING_SIZE]>
}
