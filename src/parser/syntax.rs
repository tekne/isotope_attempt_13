/*!
An experimental, extremely simple Lisp-like syntax for isotope values
*/
use nom::{
    IResult,
    combinator::{map, opt},
    branch::alt,
    sequence::{delimited, preceded, tuple},
    multi::many0,
    bytes::complete::{tag, is_not},
    character::complete::{
        multispace0 as s0
    }
};

use crate::value::{
    Value,
    ValId,
    Definition,
    Expr,
    Parametrized,
    Symbol,
    Lambda,
    Pi
};

const SPECIAL_CHARS: &str = "#() \t\r\n";

/// Parse a colon
pub fn colon(input: &str) -> IResult<&str, &str> { delimited(s0, tag(":"), s0)(input) }

/// Parse an isotope value
pub fn value<'a, S>(input: &'a str) -> IResult<&'a str, Value<S>> where &'a str: Into<S> {
    preceded(s0, map(
            tuple((def, opt(preceded(colon, value)))),
            //TODO: type assertions
            |(def, ty)| Value::new(def, ty)
    ))(input)
}

/// Parse an isotope definition
pub fn def<'a, S>(input: &'a str) -> IResult<&'a str, Definition<S>> where &'a str: Into<S> {
    alt((
        map(sexpr, |expr| expr.into()),
        map(lambda, |lambda| lambda.into()),
        map(pi, |pi| pi.into()),
        map(symbol, |symbol| symbol.into()),
    ))(input)
}

/// Parse an S-expression
pub fn sexpr<'a, S>(input: &'a str) -> IResult<&'a str, Expr<S>> where &'a str: Into<S> {
    delimited(preceded(tag("("), s0),
        map(
            many0(map(value, |value| ValId::from(value))),
            |mut v| {
                v.reverse();
                Expr { args : v.into() }.into()
            }
        ),
    preceded(s0, tag(")")))(input)
}

/// Parse a lambda function
pub fn lambda<'a, S>(input: &'a str) -> IResult<&'a str, Lambda<S>>
where &'a str: Into<S> {
    map(
        preceded(tuple((tag("#"), s0, tag("lambda"), s0)),
        parametrized
        ),
        |param| Lambda(param)
    )(input)
}

/// Parse a pi type
pub fn pi<'a, S>(input: &'a str) -> IResult<&'a str, Pi<S>>
where &'a str: Into<S> {
    map(
        preceded(tuple((tag("#"), s0, tag("pi"), s0)),
        parametrized
        ),
        |param| Pi(param)
    )(input)
}

/// Parse something parametrized
pub fn parametrized<'a, S>(input: &'a str) -> IResult<&'a str, Parametrized<S>>
where &'a str: Into<S> {
    map(
        tuple((
            delimited(
                preceded(tag("{"), s0),
                many0(map(symbol,
                    |symbol| ValId::from(Value::from(Definition::from(symbol))))),
                preceded(s0, tag("}"))
            ),
            delimited(
                s0,
                value,
                s0
            )
        )),
        |(_symbols, _value)| {
            //TODO: resolve symbols, here or later?
            unimplemented!()
        }
    )(input)
}

/// Parse a symbol
pub fn symbol<'a, S>(input: &'a str) -> IResult<&'a str, Symbol<S>> where &'a str: Into<S> {
    map(is_not(SPECIAL_CHARS), |ident: &'a str| Symbol(ident.into()))(input)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn simple_idents_parse_properly() {
        let (rest, x): (_, Value<&str>) = value("  x").expect("Syn-valid");
        assert_eq!(rest, "");
        match x.def() {
            Definition::Symbol(s) => assert_eq!(s.0, "x"),
            _ => panic!("x is a symbol!")
        }
    }
    #[test]
    fn simple_arithmetic_parses_properly() {
        let (rest, a): (_, Value<&str>) = value(" ( * w (+ x  y    ) z)").expect("Syn-valid");
        assert_eq!(rest, "");
        match a.def() {
            Definition::Expr(Expr{ args }) => {
                assert_eq!(args.len(), 4);
                match args[3].read().unwrap().def() {
                    Definition::Symbol(s) => assert_eq!(s.0, "*"),
                    _ => panic!("First arg is a symbol")
                }
                match args[2].read().unwrap().def() {
                    Definition::Symbol(s) => assert_eq!(s.0, "w"),
                    _ => panic!("Second arg is a symbol")
                }
                match args[1].read().unwrap().def() {
                    Definition::Expr(Expr { args }) => {
                        match args[2].read().unwrap().def() {
                            Definition::Symbol(s) => assert_eq!(s.0, "+"),
                            _ => panic!("First arg is a symbol")
                        }
                        match args[1].read().unwrap().def() {
                            Definition::Symbol(s) => assert_eq!(s.0, "x"),
                            _ => panic!("Second arg is a symbol")
                        }
                        match args[0].read().unwrap().def() {
                            Definition::Symbol(s) => assert_eq!(s.0, "y"),
                            _ => panic!("Third arg is a symbol")
                        }
                    }
                    _ => panic!("Third arg is an expr")
                }
                match args[0].read().unwrap().def() {
                    Definition::Symbol(s) => assert_eq!(s.0, "z"),
                    _ => panic!("Fourth arg is a symbol")
                }
            },
            _ => panic!("a is an expression!")
        }
    }
}
